'''
See LICENCE_GPL for licensing information

@author: spd
'''

from Csv import write, end_line

class Column(object):
    def __init__(self, title, key=None, func=None, sep=','):
        self.title = title
        self.key = key
        self.func = func
        self.sep = sep
    
    def write_title(self, out_file):
        self._write(out_file, self.title)
    
    def write_value(self, out_file, bug, results):
        val = ''
        if self.key is not None:
            val = bug.bug.get(self.key, '?')
        elif self.func is not None:
            val = self.func(bug)
        elif results is not None:
            result = results.get(bug.bug.bug_id)
            if result:
                val = result.get(self.title, '')
        self._write(out_file, val)
        
    def _write(self, out_file, val):
        write(out_file, val, sep=self.sep)
        if not self.sep:
            end_line(out_file)