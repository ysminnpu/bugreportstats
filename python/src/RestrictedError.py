'''
See LICENCE_GPL for licensing information

@author: spd
'''

class RestrictedError(Exception):
    pass
