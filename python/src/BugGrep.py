'''
See LICENCE_BSD for licensing information

@author: spd
'''

from Utils import parse_config
import os, re
from Csv import write, end_line

file_pattern=re.compile('\w+_\d+\.csv')
indices = [22,23]
patterns = [
    re.compile('(?i)(observed|actual)\s*(behaviou?r|results?|response)?\s*(:|\\\\n|-)'),
    re.compile('(?i)(expected|desired)\s*(behaviou?r|results?|response)?\s*(:|\\\\n|-)'),
    re.compile('(?i)(steps|reproduce|str)\s*(:|\\\\n|-)'),
]

if __name__ == '__main__':
    config = parse_config('config')
    dirs = set([config.get(p, 'sample.out_dir') for p in config.sections()])
    files = [os.path.join(d, f) for d in dirs for f in os.listdir(d) if file_pattern.match(f)]
    for f in files:
        o = os.path.join(os.path.dirname(f), os.path.basename(f).replace('.csv', '_auto.csv'))
        with open(f) as file:
            with open(o, 'w') as out:
                write(out, 'ID')
                for p, pattern in enumerate(patterns):
                    write(out, pattern.pattern, ',' if p < len(patterns) - 1 else False)
                end_line(out)
                for line in file:
                    parts = line.split(',')
                    
                    try: int(parts[0])
                    except ValueError: continue
                    
                    write(out, parts[0])
                    for p, pattern in enumerate(patterns):
                        matched = None
                        for i in indices:
                            match = pattern.search(parts[i])
                            if match:
                                matched = i
                                break
                        write(out, matched, ',' if p < len(patterns) - 1 else False)
                    end_line(out)